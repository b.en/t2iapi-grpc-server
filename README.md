# SDC-ri Provider with T2IAPI GRPC Server

This project sets out to develop a fully functional [SDC-ri](https://gitlab.com/sdc-suite/sdc-ri) Provider with [T2IAPI GRPC Server](https://github.com/Draegerwerk/t2iapi) as this combination allows testing SDC-ri for SDC-compliance using the test tool [SDCcc](https://github.com/Draegerwerk/SDCcc).

Bugs and issues discovered during the course of this project will be analysed and passed on to the respective projects (SDC-ri or SDCcc) for fixing. 

Our goal is to some day reach a state where [SDCcc](https://github.com/Draegerwerk/SDCcc) will report [SDC-ri](https://gitlab.com/sdc-suite/sdc-ri) to be fully SDC-compliant.
 

